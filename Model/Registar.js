const mongoose=require('mongoose')
const Schema =mongoose.Schema;

const objectid=Schema.objectid;

const userSchema=new Schema({
    name:{type:String,default:null,required:true},
    email:{type:String,default:null,required:true},
    password:{type:String,default:null,required:true}
},{timestamps:true})

module.exports=mongoose.model("student",userSchema)