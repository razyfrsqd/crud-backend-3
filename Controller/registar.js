var express = require("express");
var router = express.Router();
const bcrypt = require("bcryptjs");
var jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const cron = require("node-cron");

var modelreg = require("../Model/Registar");
const otp = require("../Model/Otp");
const common = require("../Otp/Otpmathod");
const salt = 10;
const secret = "1234";



cron.schedule("* * * * * ", async () => {
  const data = await otp.find({});
  data.forEach(async (element) => {
    const current = Date.now();
    const otpTime = element.createdAt.getTime();
    const minutes = Math.floor((current - otpTime) / 60000);
    if (minutes > 10) {
      const updateOtp = await otp.deleteOne({ _id: element._id });
    }
  });
  // console.log("otp-->",data)
});





exports.reg = async (req, res) => {
  console.log(req.body);
  let data = req.body;
  console.log("user detail", data);
  const isExist = await modelreg.findOne({ email: data.email });
  console.log("isexist------->", isExist);
  //isExist==true

  if (isExist) {
    res.send({ messege: "emailid is already present" });
  } else {
    bcrypt.genSalt(10, async function (err, salt) {
      bcrypt.hash(data.password, salt, async function (err, hash) {
        console.log("err----", err);
        if (err) {
          console.log(err);
        } else {
          console.log(hash);
          data.password = hash;
          // data={
          //     ...data,password:hash
          // }
          const obj2 = await modelreg.create(data);
          console.log(obj2);
          res.send({ messege: "sucessfully registered" });
        }
      });
    });
  }
};



exports.login = async (req, res) => {
  const { email, password } = req.body;
  const isExist = await modelreg.findOne({ email: email });
  console.log("---->>>", isExist);

  if (isExist) {
    bcrypt.compare(password, isExist.password, async function (err, response) {
      // res === true
      if (response) {
        //("token-->", token);

        const token = jwt.sign(
          {
            data: isExist._id,
          },
          secret,
          { expiresIn: 60 * 60 }
        );
        console.log("token-->", token);

        res.send({
          statusCode: 200,

          createRespose: isExist,
          message: "login sucess",
        });
      } else {
        res.send({ message: "password did not match" });
      }
    });
  } else {
    res.send({
      message: "User Not Verified or Invalid Credentials",
      statusCode: 204,
    });
  }
};




exports.forgotPassword = async (req, res, next) => {
  const { email } = req.body;
  const isExist = await modelreg.findOne({ email: email });
  if (isExist) {
    const otpass = await common.getOtp();
    var transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "iqbal87anwar@gmail.com",
        pass: "wacrcrrfaukybbcs",
      },
    });

    var mailOptions = {
      from: "iqbal87anwar@gmail.com",
      to: isExist.email,
      subject: "One time Password for reset your Password",
      text: `Your one time Password for reset your account on STS Admin is ${otpass}`,
    };

    transporter.sendMail(mailOptions, async function (error, info) {
      if (error) {
        console.log(error);
      } else {
        const objOtp = {
          otp: otpass,
          email: isExist.email,
          createdAt: new Date(),
        };
        const createResponse = await otp.create(objOtp);
        console.log(createResponse);
        res.send({ success: "otp sent successfully", email: isExist.email });
      }
    });
  } else {
    res.send({ error: "Email not fonund" });
  }
};





// #Admin Verify OTP Controller

exports.verifyOtp = async (req, res, next) => {
  const { otpass, email } = req.body;
  const isExist = await otp.findOne({ otp: otpass, email: email, status: "Y" });
  console.log("Otp Check", isExist);
  if (isExist) {
    // console.log(" Date",(Date.now()))
    const otpexp = Date.now();

    otpTime = isExist.createdAt.getTime();
    const minutes = Math.floor((otpexp - otpTime) / 60000);
    console.log("Check time", minutes);
    if (minutes > 10) {
      res.send({ error: "OTP expired" });
      const updateOtp = await otp.deleteOne({ email: email });
    } else {
      const updateOtp = await otp.deleteOne({ email: email });
      res.send({ success: "OTP matched", email: isExist.email, code: "200" });
    }
  } else {
    res.send({ error: "Incorrect OTP " });
  }
};





exports.resetPassword = async (req, res, next) => {
  const { email, newPassword } = req.body;
  const isExist = await modelreg.findOne({ email: email });
  console.log("NewPassword", isExist);
  if (isExist) {
    bcrypt.genSalt(10, async function (err, salt) {
      bcrypt.hash(newPassword, salt, async function (err, hash) {
        const updateResponse = await modelreg.updateOne(
          { email: email },
          { $set: { password: hash } }
        );
        console.log("NewPassword", updateResponse);
        res.send({ success: "Password Reset Successfully " });
      });
    });
  } else {
    res.send({ error: "Password not reset " });
  }
};