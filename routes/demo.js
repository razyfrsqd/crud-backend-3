var express = require("express");
var router = express.Router();
const reg2 = require("../Controller/registar");
// var modelreg=require('../Model/Registar')

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});

router.get("/student1", function (req, res, next) {
  res.send("respond with a student111");
});

router.post("/register", reg2.reg);
router.post("/login", reg2.login);
router.post("/forgotPassword", reg2.forgotPassword);
router.post("/verifyOtp", reg2.verifyOtp);

router.post("/resetPassword", reg2.resetPassword);

module.exports = router;
